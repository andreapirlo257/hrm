# Tài liệu này hướng dẫn cách chạy lệnh bằng tay trên môi trường ảo

# Trên window sẽ chạy lệnh như sau:

```sh

 > & 'C:\Program Files\Odoo 15.0.20220905\python\python.exe' .\odoo-bin -c .\odoo15-win.conf  


```