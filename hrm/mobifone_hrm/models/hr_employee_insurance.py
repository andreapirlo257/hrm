from odoo import fields, models


class HrEmployeeInsurance(models.Model):

    _name = "hr.employee"
    _inherit = "hr.employee"
    
    
    # START column Information about Employee Insurance
    insurance = fields.Char(string="Bảo hiểm")

    card_number_insurance = fields.Char(string="Số thẻ bảo hiểm y tế")

    provincial_code = fields.Char(string="Mã tỉnh cấp") 

    place_register_medical = fields.Char(string="Nơi đăng ký khám chữa bệnh") 

    insurance_info_lines = fields.One2many(comodel_name="hr.insurance.infomation.lines", inverse_name="insurance_info_id", string="Thông tin bảo hiểm")
    # END column Information about Employee Insurance

    
