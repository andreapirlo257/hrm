from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HRConfigInsurance(models.Model):

    _name = "hr.config.insurance"
    # START column Config hợp đồng lao động

    date_increase_selection = fields.Selection([
            (str(i), str(i)) for i in range(1, 29)
    ])

    date_decrease_selection = fields.Selection([
        (str(i), str(i)) for i in range(1, 29)
    ])

    wage_type = fields.Selection([
        ('bhxh', 'Lương BHXH'),
        ('bhyt', 'Lương BHYT'),
        ('other', 'Lương khác'),
    ],default='bhxh')

    config_insurance_ids = fields.One2many(
        comodel_name="hr.config.ratio.insurance", inverse_name="ratio_insurance")

    # END column Config hợp đồng lao động

    

    def next(self):    
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }
    
    def execute(self):    
        return
    
    def action_next(self):
        return self.execute() or self.next()