from datetime import datetime

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HrEmployeeContract(models.Model):

    _name = "hr.contract"
    _inherit = "hr.contract"

    # START column Information about Contract
    employee_id = fields.Many2one(
        comodel_name="hr.employee", string="Nhân viên")

    branch_contract = fields.Char(string="Chi nhánh")

    benefit = fields.Monetary(string="Mức phụ cấp")

    work_location_employee = fields.Many2one(
        comodel_name="hr.work.location", string="Địa điểm làm việc")

    employee_code = fields.Char(
        related="employee_id.employee_code", string="Mã nhân viên", readonly=False)

    _flag_first_contract = fields.Boolean(default=False)

    state_new = fields.Selection([
        ('draft', 'New'),
        ('open', 'Running'),
        ('close', 'Expired'),
    ], string='Status', group_expand='_expand_states', copy=False,
       tracking=True, help='Status of the contract', default='draft')
    
    @api.onchange('date_end', 'date_start')
    def _onchane_date(self):
        current_date = datetime.now().date()
        if self.date_end and self.date_end < current_date:
            self.state_new = 'close'
        elif self.date_end and self.date_start <= current_date:
            self.state_new = 'open'
        else:
            self.state_new = 'draft'

    @api.constrains('date_start')
    def _check_date_start(self):
        # Check if this is the first contract and its state is 'open'
        first_contract = self.env['hr.contract'].search([], limit=1)
        if first_contract and first_contract.state_new == 'open' and first_contract.id == self.id:
            return True

        # Check if the start date is greater than the end date of the previous contract
        prev_contract = self.env['hr.contract'].search([], limit=1)
        # if prev_contract and (self.date_start < prev_contract.date_end or self.date_end < prev_contract.date_end):
        if prev_contract and prev_contract.date_end and (self.date_start < prev_contract.date_end or self.date_end < prev_contract.date_end):
            raise ValidationError(_('Start and end dates must be greater than previous contract end date.'))
        return
        

    # @api.constrains('date_start')
    # def _check_date_start(self):
    #     """
    #     Kiểm tra ngày bắt đầu không được nhỏ hơn ngày kết thúc của hợp đồng trước đó
    #     """
    #     contract = self.search([('state_new', '=', 'close')], limit=1, order='date_end desc')
    #     if contract:
    #         # kiểm tra nếu hợp đồng đang được tạo là hợp đồng đầu tiên
    #         if not contract.date_end or self.date_start >= contract.date_end:
    #             return True
    #         else:
    #             raise ValidationError("Start date of new contract should be greater or equal to the end date of previous contract: %s" % (contract.date_end))
    #     else:
    #         return True

    # @api.constrains('date_start')
    # def _check_date_start(self):
    #     """
    #     Kiểm tra ngày bắt đầu không được nhỏ hơn ngày kết thúc của hợp đồng trước đó
    #     """
    #     contract = self.search([('state_new', '=', 'open')], limit=1, order='date_end desc')
    #     if contract and contract.id != self.id:  # kiểm tra không phải là hợp đồng đang được tạo mới
    #         if not contract.date_end or self.date_start >= contract.date_end:
    #             return True
    #         else:
    #             raise ValidationError("Start date of new contract should be greater or equal to the end date of previous contract: %s" % (contract.date_end))
    #     else:
    #         return True

    # END column Information about Contract
