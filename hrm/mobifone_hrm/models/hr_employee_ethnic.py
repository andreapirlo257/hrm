from odoo import fields, models


class HrEmployeeEthnic(models.Model):

    _name = "hr.employee.ethnic"
    _description = "dân tộc"

    # START column dân tộc
    name = fields.Char(string="Dân tộc")

    employee_information_general_ids = fields.One2many(
        comodel_name="hr.employee", inverse_name="ethnic_id")
    # END column dân tộc
