from odoo import fields, models


class HrEmployeeReligion(models.Model):

    _name = "hr.employee.religion"
    _description = "tôn giáo"

    # START column tình trạng hôn nhân
    name = fields.Char(string="Tôn giáo")

    employee_information_general_ids = fields.One2many(
        comodel_name="hr.employee", inverse_name="religion_id")
    # END column tình trạng hôn nhân
