import re
from datetime import date
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class InformationFamily(models.Model):

    _name = "hr.information.family.lines"
    _description = "thông tin gia đình"

    # START column Information about Information Family
    employee_information_id = fields.Many2one(
        comodel_name="hr.employee", string="Thông tin người lao động")

    relationship = fields.Char(string="Mối quan hệ", required=True)

    name_rel = fields.Char(string="Họ và tên", required=True)

    year_of_birth_rel = fields.Date(string="Năm sinh", required=True)

    job_rel = fields.Char(string="Nghề nghiệp", required=True)

    phone_rel = fields.Char(string="Số điện thoại", required=True)

    dependent_person_rel = fields.Char(string="Người phụ thuộc", required=True)
    # END column Information about Information Family

    @api.constrains('phone_rel')
    def _check_phone_rel(self):
        for rec in self:
            if rec.phone_rel:
                match = re.match('^[0-9]\d{9}$', rec.phone_rel)
                if len(rec.phone_rel) != 10:
                    raise ValidationError(_(
                        'Nhập sai định dạng số điện thoại [ %(phone)s ] trong Thông tin gia đình, người phụ thuộc. Phải bắt đầu bằng 0 và nhập đủ 10 số.',
                        phone=rec.phone_rel,
                    ))
                if not match:
                    raise ValidationError(_(
                        'Nhập sai định dạng số điện thoại [ %(phone)s ] trong Thông tin gia đình, người phụ thuộc. Phải bắt đầu bằng 0 và nhập đủ 10 số.',
                        phone=rec.phone_rel,
                    ))
                if not rec.phone_rel.startswith('0'):
                    raise ValidationError(_(
                        'Nhập sai định dạng số điện thoại [ %(phone)s ] trong Thông tin gia đình, người phụ thuộc. Phải bắt đầu bằng 0 và nhập đủ 10 số.',
                        phone=rec.phone_rel,
                    ))
        return True

    @api.constrains('year_of_birth_rel')
    def _check_dates_birth(self):
        today = date.today()
        for rec in self:
            if any(self.filtered(lambda rec: rec.year_of_birth_rel and rec.year_of_birth_rel >= today)):
                raise ValidationError(
                    _("Ngày tháng năm sinh [ %(date)s ] không được lớn hơn hoặc bằng ngày hiện tại.", date=rec.year_of_birth_ref,))
