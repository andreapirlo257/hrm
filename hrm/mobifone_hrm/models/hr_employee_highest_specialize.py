from odoo import fields, models


class HrEmployeeHighestSpecialize(models.Model):

    _name = "hr.employee.highest.specialize"
    _description = "trình độ phổ thông"

    # START column tình trạng hôn nhân
    name = fields.Char(string="chuyên môn cao nhất")

    employee_information_general_ids = fields.One2many(
        comodel_name="hr.employee", inverse_name="highest_specialize_id")
    # END column tình trạng hôn nhân
