from odoo import _, api, fields, models
from odoo.exceptions import ValidationError

class HRConfigRatioInsurance(models.Model):

    _name = "hr.config.ratio.insurance"

    # START column Information about Contract

    ratio_insurance = fields.Many2one(comodel_name="hr.config.insurance")

    so_from_month = fields.Date(string="Từ tháng")
    
    so_pay_company = fields.Float(string="CT đóng")

    so_pay_workers = fields.Float(string="NLĐ đóng")

    ac_pay_company = fields.Float(string="CT đóng")

    ac_pay_workers = fields.Float(string="NLĐ đóng")

    he_pay_company = fields.Float(string="CT đóng")

    he_pay_workers = fields.Float(string="NLĐ đóng")

    un_pay_company = fields.Float(string="CT đóng")

    un_pay_workers = fields.Float(string="NLĐ đóng")

    # END column Information about Contract
