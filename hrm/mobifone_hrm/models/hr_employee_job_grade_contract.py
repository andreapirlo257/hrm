from odoo import _, api, fields, models


class HrEmployeeJobGradeContract(models.Model):
    
    _name = "hr.employee.job.grade.contract"
    _description = "Cài đặt ngang bậc"  
    _rec_name = "name_job_grade_contract"

    # START column Information about Job Grade Contract

    stt_job_grade_contract = fields.Integer(string="STT", compute="_compute_stt")

    code_job_grade_contract = fields.Char(string="Mã ngạch", required=True)

    name_job_grade_contract = fields.Char(string="Tên ngạch", required=True)

    salary_level_ids = fields.One2many(comodel_name="hr.employee.salary.level.contract", inverse_name="job_grade_ids", store=True)

    # END column Information about Job Grade Contract

    # Tính số lượng có bao nhiêu loại hợp đồng
    @api.depends('name_job_grade_contract')
    def _compute_stt(self):
        for index, record in enumerate(self):
            record.stt_job_grade_contract = index + 1
