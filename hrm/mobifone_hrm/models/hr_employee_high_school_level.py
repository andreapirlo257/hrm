from odoo import fields, models


class HrEmployeeHighSchoolLevel(models.Model):

    _name = "hr.employee.high.school.level"
    _description = "trình độ phổ thông"

    # START column tình trạng hôn nhân
    name = fields.Char(string="Trình độ phổ thông")

    employee_information_general_ids = fields.One2many(
        comodel_name="hr.employee", inverse_name="high_school_level_id")
    # END column tình trạng hôn nhân
