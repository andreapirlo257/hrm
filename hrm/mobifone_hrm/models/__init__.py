from . import (hr_employee_contract, hr_employee_information_family,
               hr_employee_information_general, hr_employee_insurance,
               hr_employee_insurance_info_lines, hr_employee_work_experience,
               hr_employee_terms_of_contract, hr_employee_gender, hr_employee_marital_status,
               hr_employee_ethnic, hr_employee_religion, hr_employee_high_school_level,
               hr_employee_highest_specialize,hr_employee_training,hr_employee_config_file,
               hr_config_ratio_insurance,hr_employee_config_insurance,hr_employee_config_contract, hr_employee_allowances_contract,
               hr_employee_type_of_contract)
