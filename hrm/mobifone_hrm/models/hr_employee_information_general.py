import re
from datetime import date

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HrEmployeeInherit(models.Model):
    _name = "hr.employee"
    _inherit = "hr.employee"

    def _default_employee_code(self):
        try:
            today = date.today()
            year_current = today.year
            month_current = today.month
            employee = self.env["hr.employee"].search(
                [], limit=1, order='id desc')
            id_employee = int(employee.id) + 1
            return "HSNV" + "-" + str(year_current) + "-" + "{0:0=2d}".format(month_current) + "-" + "{0:0=6d}".format(id_employee)
        except:
            return "HSNV-yyyy-mm-000001"

    # START column Information about Employee
    employee_code = fields.Char(
        string="Mã nhân viên", required=True, default=_default_employee_code)

    date_of_birth = fields.Date(string="Ngày sinh", required=True)

    gender_id = fields.Many2one(
        comodel_name="hr.employee.gender", string="Giới tính", required=True)

    place_of_birth = fields.Char(string="Nơi sinh")

    address = fields.Char(string="Quê quán")

    identification = fields.Char(string="CMT/CCCD/Hộ chiếu", required=True)

    data_of_start_identification = fields.Date(string="Ngày cấp")

    place_of_identification = fields.Char(string="Nơi cấp")

    country_id = fields.Many2one(
        comodel_name="res.country", string="Quốc tịch")

    marital_id = fields.Many2one(
        comodel_name="hr.employee.marital.status", string="Tình trạng hôn nhân", store=True)

    ethnic_id = fields.Many2one(
        comodel_name="hr.employee.ethnic", string="Dân tộc", store=True)

    religion_id = fields.Many2one(
        comodel_name="hr.employee.religion", string="Tôn giáo", store=True)

    account_number = fields.Char(string="Số tài khoản")

    account_name = fields.Char(string="Tên tài khoản")

    bank = fields.Char(string="Ngân hàng")

    bank_branch = fields.Char(string="Chi nhánh ngân hàng")

    personal_tax_code = fields.Char(string="Mã số thuế cá nhân")

    date_of_entry_to_work = fields.Date(string="Ngày vào làm", required=True)

    official_start_date = fields.Date(string="Ngày vào làm chính thức")

    high_school_level_id = fields.Many2one(
        comodel_name="hr.employee.high.school.level", string="Trình độ phổ thông", store=True)

    highest_specialize_id = fields.Many2one(
        comodel_name="hr.employee.highest.specialize", string="Chuyên môn cao nhất", store=True)

    study_field = fields.Char(string="Lĩnh vực nghiên cứu")

    study_school = fields.Char(string="Trường học")

    phone_number = fields.Char(string="Số điện thoại")

    email = fields.Char(string="Email")

    accommodation_current = fields.Char(string="Chỗ ở hiện tại")

    permanent_address = fields.Char(string="Địa chỉ thường trú")

    family_information_line_ids = fields.One2many(
        comodel_name="hr.information.family.lines", inverse_name="employee_information_id")

    work_experience_line_ids = fields.One2many(
        comodel_name="hr.work.experience", inverse_name="work_experience_employee_id")
    
    employee_traning_line_ids = fields.One2many(
        comodel_name="hr.information.training", inverse_name="employee_traning_id")

    contract_ids = fields.One2many(
        comodel_name="hr.contract", inverse_name="employee_id")

    contract_job_ids_tree = fields.One2many(
        comodel_name="hr.contract", inverse_name="employee_id")

    contract_job_ids = fields.One2many(
        comodel_name="hr.contract", inverse_name="employee_id", domain=[('state_new', '=', 'open')], )

    job_id_contract = fields.Many2one(
        related="contract_job_ids.job_id", string="Vị trí làm việc")

    name_contract_id = fields.Char(
        related="contract_job_ids.name", string="Mã hợp đồng lao động")

    contract_type_contract_id = fields.Many2one(
        related="contract_job_ids.contract_type_id", string="Kiểu hợp đồng lao động")

    wage_contract = fields.Monetary(
        related="contract_job_ids.wage", currency_field="currency_id")

    currency_id = fields.Many2one(
        comodel_name="res.currency", string="Currency", default=lambda self: self.env.company.currency_id)

    department_contract_id = fields.Many2one(
        related="contract_job_ids.department_id", string="Phòng/Ban (hợp đồng)")

    branch_contract = fields.Char(
        related="contract_job_ids.branch_contract", string="Chi nhánh")

    work_location_employee = fields.Many2one(
        related="contract_job_ids.work_location_employee", string="Địa điểm làm việc")

    child_ids = fields.One2many(
        comodel_name="hr.employee", inverse_name="parent_id", string="Cấp dưới trực tiếp")

    department_id = fields.Many2one('hr.department', string="Phòng/Ban",
                                    domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")

    work_phone = fields.Char('Điện thoại công ty',
                             default="0", store=True, readonly=False)

    mobile_phone = fields.Char('Số di động làm việc', default="0")

    work_email = fields.Char('Email công việc')

    coach_id = fields.Many2one(
        'hr.employee', 'Người huấn luyện', compute='_compute_coach', store=True, readonly=False,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        help='Select the "Employee" who is the coach of this employee.\n'
        'The "Coach" has no specific rights or responsibilities by default.')

    parent_id = fields.Many2one('hr.employee', 'Người quản lý', compute="_compute_parent_id", store=True, readonly=False,
                                domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")

    _sql_constraints = [
        ('employee_code_unique', 'unique (employee_code)', 'Mã nhân viên đã tồn tại.')]
    
    sequence_hr_employee = fields.Integer(string="STT", compute="_compute_stt")

    @api.depends('employee_code')
    def _compute_stt(self):
        for index, record in enumerate(self):
            record.sequence_hr_employee = index + 1
    
    # union_member_ids = fields.One2many(comodel_name="hr.trade.union.members", inverse_name="employee_id")
    # END column Information about Employee

    @api.model
    def create(self, vals):
        record = super(HrEmployeeInherit, self).create(vals)
        self.env["hr.trade.union.approval"].create({
            'employee_id': vals['resource_id'],
        })
        return record

    @api.constrains("employee_code")
    def _check_employee_code(self):
        if self.employee_code:
            pattern = r"^HSNV-\d{4}-(0[1-9]|1[0-2])-\d{6}$"
            if not re.match(pattern, self.employee_code):
                raise ValidationError(
                    _("Mã nhân viên yêu cầu đúng định dạng HSNV-yyyy-mm-000001."))

    @api.constrains("date_of_birth")
    def _check_dates_birth(self):
        today = date.today()
        if any(self.filtered(lambda rec: rec.date_of_birth and rec.date_of_birth >= today)):
            raise ValidationError(
                _("Ngày tháng năm sinh không được lớn hơn hoặc bằng ngày hiện tại."))

    @api.constrains("personal_tax_code")
    def _check_personal_tax_code(self):
        if self.personal_tax_code:
            match = re.match('^[0-9]', self.personal_tax_code)
            if not match:
                raise ValidationError(
                    _("Mã số thuế cá nhân vui lòng chỉ nhập số."))

    @api.constrains("account_number")
    def _check_account_number(self):
        if self.account_number:
            match = re.match('^[0-9]', self.account_number)
            if not match:
                raise ValidationError(
                    _("Số tài khoản vui lòng chỉ nhập số."))

    @api.constrains("identification")
    def _check_identification(self):
        if self.identification:
            match = re.match('^[0-9]', self.identification)
            if not match:
                raise ValidationError(
                    _("CMT/CCCD/Hộ chiếu vui lòng chỉ nhập số."))

    @api.constrains("phone_number")
    def _check_phone_number(self):
        for rec in self:
            if rec.phone_number:
                match = re.match('^[0-9]\d{9}$', rec.phone_number)
                if len(rec.phone_number) != 10:
                    raise ValidationError(_(
                        'Nhập sai định dạng số điện thoại [ %(phone)s ] ở thông tin liên hệ. Phải bắt đầu bằng 0 và nhập đủ 10 số.',
                        phone=rec.phone_number,
                    ))
                if not match:
                    raise ValidationError(_(
                        'Nhập sai định dạng số điện thoại [ %(phone)s ] ở thông tin liên hệ. Phải bắt đầu bằng 0 và nhập đủ 10 số.',
                        phone=rec.phone_number,
                    ))
                if not rec.phone_number.startswith('0'):
                    raise ValidationError(_(
                        'Nhập sai định dạng số điện thoại [ %(phone)s ] ở thông tin liên hệ. Phải bắt đầu bằng 0 và nhập đủ 10 số.',
                        phone=rec.phone_number,
                    ))
        return True

    @api.constrains("email")
    def _check_email_format(self):
        for record in self:
            if record.email and not re.match(r"[^@]+@[^@]", record.email):
                raise ValidationError(_("Vui lòng nhập dúng định dạng Email."))

    @api.constrains("date_of_entry_to_work", "official_start_date")
    def _check_dates(self):
        for rec in self:
            if any(rec.filtered(lambda rec: rec.date_of_entry_to_work and rec.official_start_date and rec.date_of_entry_to_work >= rec.official_start_date)):
                raise ValidationError(
                    _("Ngày vào làm chính thức [ %(date_work)s ] phải lớn hơn hoặc bằng ngày vào làm [ %(date_offi)s ].", date_work=rec.date_of_entry_to_work, date_offi=rec.official_start_date))
        return True
