from odoo import _, api, fields, models


class HrEmployeeAllowancesContract(models.Model):
    
    _name = "hr.employee.allowances.contract"
    _description = "Các loại phụ cấp"  
    _rec_name = "name_allowances_contract"

    # START column Information about Job Grade Contract

    stt_type_of_contract = fields.Integer(string="STT", compute="_compute_stt")

    name_allowances_contract = fields.Char(string="Tên phụ cấp", required=True)


    @api.depends('name_allowances_contract')
    def _compute_stt(self):
        for index, record in enumerate(self):
            record.stt_type_of_contract = index + 1

    