from odoo import _, api, fields, models


class HrEmployeeTypeOfContract(models.Model):
    
    _name = "hr.employee.type.of.contract"
    _description = "Loại hợp đồng lao động"  
    _rec_name = "name_type_of_contract"

    # START column Information about Type Of Contract
    
    stt_type_of_contract = fields.Integer(string="STT", compute="_compute_stt")

    name_type_of_contract = fields.Char(string = "Tên loại hợp đồng lao động", required=True)

    abbreviation_code_contract = fields.Char(string = "Mã viết tắt", required=True)

    type_contract = fields.Selection(selection=[('seasonal_contracts','Hợp đồng thời vụ'),
        ('fixed_term_contract','Hợp đồng xác định thời gian'),
        ('indefinite_contract','Hợp đồng không xác định thời gian')], string="Hình thức hợp đồng")

    insurance_premiums_contract = fields.Selection(selection=[('yes','Có'),('no','Không')], string="Đóng bảo hiểm")

    time_form_contract = fields.Selection(selection=[('full_time','Toàn thời gian'),('part_time','Bán thời gian')], required=True, string="Hình thức thời gian")

    duration_contract = fields.Integer(string="Thời hạn (tháng)", 
                                        help="Thời hạn hợp đồng được tính bằng tháng")

    duration_contract_display = fields.Char(string="Thời hạn (tháng)",
                                        compute='_compute_duration_contract_display', 
                                        store=False)

    # END column Information about Type Of Contract

    # Tính số lượng có bao nhiêu loại hợp đồng
    @api.depends('name_type_of_contract')
    def _compute_stt(self):
        for index, record in enumerate(self):
            record.stt_type_of_contract = index + 1
    
    # duration_contract_display sẽ hiển thị giá trị của trường "duration_contract" cộng với đơn vị "tháng" 
    @api.depends('duration_contract')
    def _compute_duration_contract_display(self):
        for contract in self:
            contract.duration_contract_display = str(contract.duration_contract) + ' tháng'