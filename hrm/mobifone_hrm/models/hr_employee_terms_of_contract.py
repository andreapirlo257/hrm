from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HRTermOfContract(models.Model):

    _name = "hr.terms.of.contract"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "điều khoản hợp đồng"
    _rec_name = "name_terms_of_contract"

    # START column Information about Terms of contract
    name_terms_of_contract = fields.Char(
        string="Tên điều khoản hợp đồng", required=True)

    state = fields.Boolean(
        string="Trạng thái", default=True)

    state_display = fields.Char(
        string="Trạng thái hiển thị", compute="_compute_status_display")

    apply_for = fields.Selection(
        selection="list_apply_for", string="Áp dụng cho", required=True)

    contents_of_terms = fields.Html(string="Nội dung của điều khoản", required=True)
    # END column Information about Terms of contract

    def list_apply_for(self):
        return [("HDLD", "Hợp đồng lao động"), ("HDDTN", "Hợp đồng đào tạo nghề"), ("HDCMNV", "Hợp đồng chuyên môn nghiệp vụ")]

    @api.constrains("apply_for", "state")
    def _check_apply_for(self):
        self_selection = len(self.env["hr.terms.of.contract"].search(
            ['&', ('apply_for', '=', self.apply_for), ('state', '=', True)]).exists())
        if self_selection > 1:
            raise ValidationError(
                _("Đã có điều khoản [%(name_terms_of_contract)s] áp dụng cho mẫu in [%(apply_for)s]. Vui lòng chọn lại loại mẫu in hợp đồng áp dụng điều khoản này.", name_terms_of_contract=self.name_terms_of_contract, apply_for=dict(self._fields['apply_for']._description_selection(self.env)).get(self.apply_for)))
        return True

    @api.depends("state")
    def _compute_status_display(self):
        for record in self:
            if record.state:
                record.state_display = "Đang áp dụng"
            else:
                record.state_display = "Đã hủy"

    def unlink(self):
        for record in self:
            record.state = False

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Tải về mẫu cho điều khoản hợp đồng'),
            'template': '/mobifone_hrm/static/src/file_template/helpers.xlsx'
        }]
