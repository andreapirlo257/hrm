from odoo import _, api, fields, models


class HrEmployeeSalaryLevelContract(models.Model):
    
    _name = "hr.employee.salary.level.contract"
    _description = "Bậc lương"  
    _rec_name = "name_salary_level"

    # def _default_salary_level(self):
    #     count = len(self.search([]))
    #     return 'Bậc %d' % (count + 1)

    # START column Information about Salary Level Contract

    job_grade_ids = fields.Many2one(comodel_name="hr.employee.job.grade.contract")

    name_salary_level = fields.Char(string="Bậc lương", invisible=True)

    salary_structure = fields.Char(string="Hình thức lương 1")

    salary_coefficient = fields.Integer(string="Hệ số 1")

    salary_amount = fields.Integer(string="Số tiền HTL1")    

    salary_structure_2 = fields.Char(string="Hình thức lương 2")

    salary_coefficient_2 = fields.Integer(string="Hệ số 2")

    salary_amount_2 = fields.Integer(string="Số tiền HTL2")

    # def create(self, cr, uid, vals, context=None):
    #     sequence=self.pool.get('ir.sequence').get(cr, uid, 'reg_code')
    #     vals['name_salary_level']= sequence
    #     return super(HrEmployeeSalaryLevelContract, self).create(cr, uid, vals, context=context)
    # @api.model
    # def create(self, vals):
    #     seq = self.env['ir.sequence'].next_by_code('hr.employee.salary.level.contract')
    #     vals['name_salary_level'] = seq
    #     return super(HrEmployeeSalaryLevelContract, self).create(vals)

