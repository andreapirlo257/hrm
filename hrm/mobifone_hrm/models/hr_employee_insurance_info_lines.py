from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class InsurancePaymentInformation(models.Model):

    _name = "hr.insurance.infomation.lines"
    _description = "thông tin thanh toán bảo hiểm"


    # START column Information about Payment Information
    insurance_info_id = fields.Many2one(comodel_name="hr.employee", string="Thông tin đóng bảo hiểm")

    so_from_month = fields.Date(string="Từ tháng")

    so_formality = fields.Char(string="Hình thức")

    so_reason = fields.Char(string="Lý do")

    so_pay_company = fields.Float(string="%CT đóng \n Bảo hiểm xã hội")

    so_pay_workers = fields.Float(string="%NLĐ đóng \n Bảo hiểm xã hội")

    ac_pay_company = fields.Float(string="%CT đóng \n Bảo hiểm TNLĐ")

    ac_pay_workers = fields.Float(string="%NLĐ đóng \n Bảo hiểm TNLĐ")

    he_pay_company = fields.Float(string="%CT đóng \n Bảo hiểm y tế")

    he_pay_workers = fields.Float(string="%NLĐ đóng \n Bảo hiểm y tế")

    un_pay_company = fields.Float(string="%CT đóng \n Bảo hiểm thất nghiệp")

    un_pay_workers = fields.Float(string="%NLĐ đóng \n Bảo hiểm thất nghiệp")

    insurance_premium_rate = fields.Integer(string="Mức đóng BH")

    un_pay_company_total = fields.Integer(string="Công ty đóng", compute='_compute_un_pay_company_total')

    un_pay_workers_total = fields.Integer(string="NLĐ đóng", compute='_compute_un_pay_workers_total')
    # END column Information about Payment Information


    @api.constrains("so_from_month", "so_formality", "so_reason", "so_pay_company", "so_pay_workers", "ac_pay_company", "ac_pay_workers", "he_pay_company", "he_pay_workers", "un_pay_company", "un_pay_workers")
    def _check_payment_values(self):
        for record in self: 
            if not (0 <= record.so_pay_company <= 1): 
                raise ValidationError(_("Vui lòng nhập lại CT đóng của Bảo hiểm xã hội\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.so_pay_workers <= 1): 
                raise ValidationError(_("Vui lòng nhập lại NLĐ đóng của Bảo hiểm xã hội\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.ac_pay_company <= 1): 
                raise ValidationError(_("Vui lòng nhập lại CT đóng của Bảo hiểm TNLĐ\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.ac_pay_workers <= 1): 
                raise ValidationError(_("Vui lòng nhập lại NLĐ đóng của Bảo hiểm TNLĐ\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.he_pay_company <= 1): 
                raise ValidationError(_("Vui lòng nhập lại CT đóng của Bảo hiểm y tế\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.he_pay_workers <= 1): 
                raise ValidationError(_("Vui lòng nhập lại NLĐ đóng của Bảo hiểm y tế\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.un_pay_company <= 1): 
                raise ValidationError(_("Vui lòng nhập lại CT đóng của Bảo hiểm thất nghiệp\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.un_pay_workers <= 1): 
                raise ValidationError(_("Vui lòng nhập lại NLĐ đóng của Bảo hiểm thất nghiệp\nGiá trị nhập vào chỉ từ 0 đến 100%"))
                      
    @api.depends("un_pay_company_total","so_pay_company","ac_pay_company","he_pay_company","un_pay_company", "insurance_premium_rate")
    def _compute_un_pay_company_total(self):
        for record in self:
            record.un_pay_company_total = ((record.so_pay_company + record.ac_pay_company + record.he_pay_company + record.un_pay_company) * record.insurance_premium_rate) / 1

    @api.depends("un_pay_workers_total","so_pay_workers","ac_pay_workers","he_pay_workers","un_pay_workers", "insurance_premium_rate")
    def _compute_un_pay_workers_total(self):
        for record in self:
            record.un_pay_workers_total = ((record.so_pay_workers + record.ac_pay_workers +record.he_pay_workers + record.un_pay_workers) * record.insurance_premium_rate) / 1