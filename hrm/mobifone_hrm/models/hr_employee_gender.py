from odoo import fields, models


class HrEmployeeGender(models.Model):

    _name = "hr.employee.gender"
    _description = "giới tính nhân viên"

    # START column giới tính nhân viên
    name = fields.Char(string="Giới tính")

    employee_information_general_ids = fields.One2many(
        comodel_name="hr.employee", inverse_name="gender_id")
    # END column giới tính nhân viên
