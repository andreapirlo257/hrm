from odoo import fields, models


class HrEmployeeMaritalStatus(models.Model):

    _name = "hr.employee.marital.status"
    _description = "tình trạng hôn nhân"

    # START column tình trạng hôn nhân
    name = fields.Char(string="Tình trạng hôn nhân")

    employee_information_general_ids = fields.One2many(
        comodel_name="hr.employee", inverse_name="marital_id")
    # END column tình trạng hôn nhân
