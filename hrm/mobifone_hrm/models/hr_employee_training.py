import re
from datetime import date
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class InformationTraining(models.Model):

    _name = "hr.information.training"
    _description = "thông tin gia đình"

    # START column Information Training
    employee_traning_id = fields.Many2one(
        comodel_name="hr.employee", string="Thông tin đào tạo")

    from_day = fields.Date(string= "Từ ngày", required=True)

    end_day = fields.Date(string = "Đến ngày", required=True)

    traning_course = fields.Char(string="Khóa đào tạo/chương trình học", required=True)

    grade = fields.Char(string="Xếp loại")    
    
    upload = fields.Binary(string="Bằng cấp/Giấy chứng nhận")

    upload_name = fields.Char(string="Bằng cấp/Giấy chứng nhận")

    referencer = fields.Char(string="Người tham chiếu", required=True)

    traning_content = fields.Char(string="Nội dung đào tạo", required=True)

    # END column Information about Information Training
    
