from datetime import date
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class WorkExperience(models.Model):
    
    _name="hr.work.experience"
    _description="kinh nghiệm làm việc"  


    # START column Information about Work Experience
    work_experience_employee_id = fields.Many2one(comodel_name="hr.employee", string="Kinh nghiệm làm việc")

    from_day_work = fields.Date(string= "Từ ngày", required=True)

    end_day_work = fields.Date(string = "Đến ngày", required=True)

    position_work = fields.Char(string = "Vị trí làm việc", required=True)

    reference_work = fields.Char(string = "Người tham chiếu", required=True)

    description_work = fields.Char(string="Mô tả công việc", required=True)

    company_work = fields.Char(string ="Công ty", required=True)
    # START column Information about Work Experience


    @api.constrains("from_day_work", "end_day_work")
    def _check_dates(self):
        today = date.today()
        for rec in self:
            if any(rec.filtered(lambda rec: rec.from_day_work and rec.end_day_work and rec.from_day_work >= rec.end_day_work)):
                raise ValidationError(
                    _("Ngày bắt đầu [ %(from_day)s ] phải trước Ngày kết thúc [ %(end_day)s ].", from_day=rec.from_day_work, end_day=rec.end_day_work))
            if any(rec.filtered(lambda rec: rec.from_day_work and rec.from_day_work >= today)):
                raise ValidationError(
                    _("Ngày bắt đầu [ %(from_day)s ] không được lớn hơn ngày hiện tại.", from_day=rec.from_day_work, end_day=rec.end_day_work))
            elif any(rec.filtered(lambda rec: rec.end_day_work and rec.end_day_work > today)):
                raise ValidationError(
                    _("Ngày kết thúc [ %(end_day)s ] không được lớn hơn ngày hiện tại.", from_day=rec.from_day_work, end_day=rec.end_day_work))
            return True