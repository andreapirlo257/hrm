{
    "name": "MBF Human Resource",

    "category": "Human resource",

    "summary": "MobiFone human resource management system",

    "sequence": 0,

    "description": "MobiFone human resource management system",

    "version": "0.1",

    "author": "MobifoneIT - PTPM3",

    "website": "https://dev3.1erp.vn",

    "company": 'MobifoneIt - PM3',

    "maintainer": 'MobifoneIt -PM3',

    "depends": ["mail", "hr", "hr_contract", "hr_holidays", "hr_attendance", "mobifone_theme"],

    "data": [
        'security/ir.model.access.csv',
        'data/list_ethnic.xml',
        'data/list_gender.xml',
        'data/list_high_school_level.xml',
        'data/list_highest_specialize.xml',
        'data/list_marital.xml',
        'data/list_religion.xml',
        'views/employee_information_general_view.xml',
        'views/employee_information_family_view.xml',
        'views/employee_insurance_info_lines_view.xml',
        'views/employee_insurance_view.xml',
        'views/employee_job_view.xml',
        'views/employee_contract_view.xml',
        'views/employee_work_experience_view.xml',
        'views/employee_training_view.xml',
        'views/employee_terms_of_contract_view.xml',
        'views/contract_view.xml',
        'views/employee_config_file.xml',
        'views/employee_config_insurance.xml',
        'views/employee_config_contract.xml',
        'views/employee_allowances_contract_view.xml',
        'views/employee_type_of_contract_view.xml',
        'views/employee_menu.xml',
        'report/report_common.xml',
        'report/report_labor_contract.xml',
        'report/report_professional_contract.xml',
        'report/report_template.xml',
        'report/report_vocational_training_contract.xml',
    ],

    "demo": [],

    'assets': {
        'web.assets_frontend': [
            'mobifone_hrm/static/src/scss/report_contract.scss'
        ],
        'web.assets_backend': [
            'mobifone_hrm/static/src/scss/employee_information_general.scss',
            'mobifone_hrm/static/src/scss/employee_insurance.scss',
            'mobifone_hrm/static/src/scss/employee_type_of_contract.scss',
            'mobifone_hrm/static/src/scss/employee_job_grade_contract.scss',
            'mobifone_hrm/static/src/scss/employee_term_of_contract.scss',
        ],
        'web.assets_qweb': [
        ],
        'web.report_assets_common': [
            'mobifone_hrm/static/src/scss/report_contract.scss',
        ],
    },
    "license": "LGPL-3",

    "installable": True,

    "application": True,

    "auto_install": False,
}
