# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ApprovalWizard(models.TransientModel):
    _name = "hr.trade.union.approval.wizard"
    _description = "Union approval wizard"

    # START column Information about ApprovalWizard

    name = fields.Char(readonly=True)

    employee_id = fields.Many2one(comodel_name="hr.employee")

    trade_union_code = fields.Char(string="Mã đoàn viên")

    trade_union_position = fields.Many2one(comodel_name='hr.trade.union.department', string="Chức vụ công đoàn")

    trade_union_organization = fields.Many2one(
        comodel_name='hr.trade.union.organization', string="Tổ chức công đoàn")
    
    date_join_trade_union = fields.Date(default=fields.Date.today(), string="Ngày vào công đoàn")

    trade_union_status = fields.Selection([
            ('active', 'Hoạt động'),
            ('non', 'Không hoạt động')
        ], default='active', readonly=True, string="Tình trạng hoạt động")
    
    trade_union_approval_id = fields.Many2one(comodel_name="hr.trade.union.approval")

    # END column Information about ApprovalWizard

    @api.model
    def create(self, info_trade_union_wizard):
        trade_union_approval_obj = self.env['hr.trade.union.approval'].search([('id', '=', info_trade_union_wizard["trade_union_approval_id"])], limit=1)
        value = {
            'trade_union_code': info_trade_union_wizard["trade_union_code"],
            'trade_union_position': info_trade_union_wizard["trade_union_position"],
            'trade_union_organization': info_trade_union_wizard["trade_union_organization"],
            'date_join_trade_union': info_trade_union_wizard["date_join_trade_union"],
            'trade_union_status': 'active',
        }
        trade_union_approval_obj.write(value)

        
        return super(ApprovalWizard,self).create(info_trade_union_wizard)


    def action_launch(self):
        try: 
            trade_union_members = self.env['hr.trade.union.approval'].search([('id', '=', self.trade_union_approval_id.id)], limit=1)
            trade_union_members.trade_union_state = "approval"
            if trade_union_members:
                value_trade_union_member = {
                    "name":trade_union_members.name,
                    "trade_union_organization":trade_union_members.trade_union_organization.id,
                    "trade_union_position":trade_union_members.trade_union_position.id,
                    "trade_union_code":trade_union_members.trade_union_code,
                    "date_join_trade_union": trade_union_members.date_join_trade_union,
                    "trade_union_status": trade_union_members.trade_union_status,
                    "employee_id": trade_union_members.employee_id.id
                }
                self.env['hr.trade.union.members'].create(value_trade_union_member)
                trade_union_members.unlink()

                next_record = self.env['hr.trade.union.approval'].search([], limit=1)
                if next_record:
                    return {
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'hr.trade.union.approval',
                        'res_id': next_record.id,
                        'target': 'current',
                        'clear_breadcrumbs': True,
                    }
                else:
                    return {
                        'type': 'ir.actions.act_window',
                        'view_mode': 'tree',
                        'res_model': 'hr.trade.union.members',
                        'views': [(False, 'tree')],
                        'target': 'current',
                        'clear_breadcrumbs': True,
                    }
        except Exception as e:
            raise e
                
