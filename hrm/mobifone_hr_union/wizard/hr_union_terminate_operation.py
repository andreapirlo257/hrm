from odoo import fields, models


class HrTradeUnionTerminateOperationWizard(models.TransientModel):

    _name = 'hr.trade.union.terminate.operation.wizard'
    _description = 'Lưu trữ dữ liệu đoàn viên'

     # Trả về ngày mặt định
    def _get_default_date_terminate_operation_union(self):
        date_terminate_operation_union = False
        if self.env.context.get('active_id'):
            date_terminate_operation_union = self.env['hr.trade.union.members'].browse(self.env.context['active_id']).date_terminate_operation_union
        return date_terminate_operation_union or fields.Date.today()

    # START column Information about hr.trade.union.terminate.operation.wizard

    description_terminate_operation_union = fields.Html(string="Lý do chi tiết")

    date_terminate_operation_union = fields.Date(string="Ngày ngưng hoạt động", required=True, default=_get_default_date_terminate_operation_union)
    
    reason_terminate_operation_union = fields.Many2one(comodel_name="hr.trade.union.reason", string="Lý do ngừng hoạt động")

    activity_one = fields.Boolean(string="Hoạt động 1")
    
    activity_two = fields.Boolean(string="Hoạt động 2")

    activity_three = fields.Boolean(string="Hoạt động 3")

    activity_four = fields.Boolean(string="Hoạt động 4")

    union_terminate_operation_id = fields.Many2one(
        'hr.trade.union.members', required=True,
        default=lambda self: self.env.context.get('active_id', None))
    
    # END column Information about hr.trade.union.terminate.operation.wizard

    def action_register_departure(self):
        union = self.union_terminate_operation_id
        if self.env.context.get('toggle_active', False) and union.active:
            union.with_context(no_wizard=True).toggle_active()
        union.description_terminate_operation_union = self.description_terminate_operation_union
        union.date_terminate_operation_union = self.date_terminate_operation_union
        union.reason_terminate_operation_union = self.reason_terminate_operation_union
        union.activity_one = self.activity_one
        union.activity_two = self.activity_two
        union.activity_three = self.activity_three
        union.activity_four = self.activity_four
        