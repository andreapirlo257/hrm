{
    "name": "Trade Union Organization",

    "category": "Trade union",

    "summary": "Trade union organization",

    'sequence': -101,

    "description": "Trade union organization",

    "version": "0.1",

    "author": "MobifoneIT - PTPM3",

    "website": "https://dev3.1erp.vn",

    "company": 'MobifoneIt - PM3',

    "maintainer": 'MobifoneIt -PM3',

    "depends": ["mobifone_hrm"],

    "data": [
        'security/ir.model.access.csv',
        'wizard/hr_approval.xml',
        'wizard/hr_union_terminate_operation.xml',
        'views/hr_union_members.xml',
        'views/hr_union_organization.xml',
        'views/hr_union_approval.xml',
        'views/hr_union_departure_reason.xml',
        'views/hr_union_department.xml',
        'views/hr_merge_union_organization.xml',
        'views/menu.xml',
    ],
    "demo": [],

    'assets': {
        'web.assets_frontend': [],
        'web.assets_backend': [
            'mobifone_hr_union/static/src/scss/hr_union.scss',
            'mobifone_hr_union/static/src/js/hr_union_archive.js'
        ],
        'web.assets_qweb': [],
    },
    "license": "LGPL-3",

    "installable": True,

    "application": True,

    "auto_install": False,

}
