from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HrTradeUnionOrganization(models.Model):

    _name = "hr.trade.union.organization"
    _description = "Tổ chức công đoàn"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _rec_name = "complete_name"

    # START column Union Organization

    id_trade_union_organization = fields.Char(required=True)

    name_trade_union_organization = fields.Char(required=True)

    parent_trade_union_organization_id = fields.Many2one(
        comodel_name="hr.trade.union.organization", string="Tổ chức công đoàn cấp trên", index=True)

    complete_name = fields.Char(
        compute="_compute_complete_name", recursive=True, store=True)

    child_ids = fields.One2many(comodel_name="hr.trade.union.organization",
                                inverse_name="parent_trade_union_organization_id")

    color = fields.Integer(string='Chỉ số màu')

    order_same_level_unionOrganization = fields.Char(string="Thứ tự cùng cấp")

    absence_of_today = fields.Integer(string='Ngưng hoạt động')

    des_trade_union_organization = fields.Char(string="Mô tả")

    level_trade_union_organization = fields.Integer(
        string="Level", default=1, readonly=True)

    date_establish_trade_union_organization = fields.Date(
        string="Ngày thành lập", required=True)

    date_end_establish_trade_union_organization = fields.Date(
        string="Ngày ngừng hoạt động")

    status_trade_union_organization = fields.Selection(selection=[('apply', 'Hoạt động'), (
        'none_apply', 'Không hoạt động')], string="Trạng thái", default="apply", attrs={'readonly': [('create', True)]})

    state_display = fields.Char(compute="_compute_status_display")

    hr_responsible_id = fields.Many2one(
        'res.users', tracking=True, string="Người quản lý")

    department_id = fields.Many2one(
        comodel_name="hr.trade.union.department", domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")

    union_member_count = fields.Integer(
        compute="_compute_trade_union_member_count")

    union_member_ids = fields.One2many(
        comodel_name="hr.trade.union.members", inverse_name='trade_union_organization')

    total_employee = fields.Integer(compute='_compute_total_employee')

    total_inactive_employee = fields.Integer(compute='_compute_total_inactive_employee')

    # END column Union Organization

    # The function to calculate the total number of hr.trade.union.members
    def _compute_total_employee(self):
        for rec in self:
            active_employee_count = self.env["hr.trade.union.members"].search_count([
                ("trade_union_organization", "=", rec.id),("active", "=", True),])
            inactive_employee_count = self.env["hr.trade.union.members"].search_count([
                ("trade_union_organization", "=", rec.id),("active", "=", False),])
            total_employee_count = active_employee_count + inactive_employee_count
            rec.total_employee = total_employee_count
    
    # The function calculates the total number of employees whose status is active = False
    @api.depends("union_member_ids.active")
    def _compute_total_inactive_employee(self):
        for rec in self:
            inactive_employee_count = self.env["hr.trade.union.members"].search_count([
                ("trade_union_organization", "=", rec.id),("active", "=", False)])
            rec.total_inactive_employee = inactive_employee_count

    # The function to calculate the total number of employees with active status = True
    @api.depends("union_member_ids")
    def _compute_trade_union_member_count(self):
        for rec in self:
            rec.union_member_count = len(rec.union_member_ids)

    # The function automatically increases the level_trade_union_organization by 1
    @api.onchange("parent_trade_union_organization_id")
    def _onchange_parent_union_org(self):
        if self.parent_trade_union_organization_id:
            self.level_trade_union_organization = self.parent_trade_union_organization_id.level_trade_union_organization + 1
        else:
            self.level_trade_union_organization = 1

    # Function Delete when there is no member
    def unlink(self):
        for rec in self:
            if rec.union_member_count and rec.union_member_count > 0:
                raise ValidationError(_("You can not delete"))
        return super(HrTradeUnionOrganization, self).unlink()

    @api.depends("status_trade_union_organization")
    def _compute_status_display(self):
        for record in self:
            if record.status_trade_union_organization and record.status_trade_union_organization == 'apply':
                record.state_display = "Hoạt động"
            else:
                record.state_display = "Ngừng hoạt động"
    
    @api.depends("name_trade_union_organization", "parent_trade_union_organization_id.complete_name")
    def _compute_complete_name(self):
        for department in self:
            if department.parent_trade_union_organization_id:
                department.complete_name = '%s / %s' % (
                    department.parent_trade_union_organization_id.complete_name, department.name_trade_union_organization)
            else:
                department.complete_name = department.name_trade_union_organization
