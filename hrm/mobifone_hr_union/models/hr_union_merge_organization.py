from odoo import _, fields, models


class HrTradeUnionMergeOrganization(models.Model):
    _name = "hr.trade.union.merge.organization"
    _description = "Hợp nhất tổ chức công đoàn"

    merger_union = fields.Many2one(comodel_name="hr.trade.union.organization", string="TCCĐ cần sáp nhập")
