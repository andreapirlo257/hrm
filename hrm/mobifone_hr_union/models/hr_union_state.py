import re
from datetime import date

from odoo import _, api, fields, models


class MergeUnionOrganization(models.Model):
    _name = "union.organization.state"
    _description = "Union Organization State"

    state = fields.Char(string="Trạng thái")