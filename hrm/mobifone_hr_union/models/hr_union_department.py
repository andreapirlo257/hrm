from odoo import _, api, fields, models


class HrTradeUnionDepartment(models.Model):

    _name = "hr.trade.union.department"
    _description = "Tổ chức công đoàn"
    _inherit = ['mail.thread']
    _order = "name"

    # START column Hr Trade Union Department
    name = fields.Char('Chức vụ công đoàn', required=True)

    active = fields.Boolean(string="Trạng thái", default=True)

    description = fields.Text(string="Mô tả")

    union_member_ids = fields.One2many(comodel_name="hr.trade.union.members", inverse_name='trade_union_position')

    total_employee = fields.Integer(
        compute="_compute_total_employee")

    # END column Hr Trade Union Department

    def get_name(self):
        if not self.env.context.get('hierarchical_naming', True):
            return [(record.id, record.name) for record in self]
        return super(HrTradeUnionDepartment, self).get_name()

    @api.model
    def name_create(self, name):
        return self.create({'name': name}).get_name()[0]

    @api.depends("union_member_ids")
    def _compute_total_employee(self):
        for rec in self:
            rec.total_employee = len(rec.union_member_ids)