from odoo import _, fields, models


class HrTradeUnionMembers(models.Model):

    _name = "hr.trade.union.members"
    _description = "Thành viên công đoàn"
    _rec_name = "name"
    
    # START column Hr Trade Union Members
    
    employee_id = fields.Many2one(comodel_name="hr.employee")

    trade_union_state = fields.Selection([
    ('pending', 'Chờ phê duyệt'),
    ('approval', 'Đã phê duyệt')
    ], string="Trạng thái")

    gender_id = fields.Many2one(related='employee_id.gender_id', string="Giới tính")

    image_1024 = fields.Image(related='employee_id.image_1024', compute_sudo=True)

    avatar_128 = fields.Image("Avatar 1024", related='employee_id.avatar_1024', compute_sudo=True)

    name = fields.Char(readonly=True, string="Tên đoàn viên")

    trade_union_position = fields.Many2one(comodel_name='hr.trade.union.department', string="Chức vụ công đoàn")

    trade_union_code = fields.Char(string="Mã đoàn viên")

    employee_code = fields.Char(related='employee_id.employee_code', string="Mã nhân viên")

    trade_union_organization = fields.Many2one(
        comodel_name='hr.trade.union.organization', string="Tổ chức công đoàn")
    
    name_trade_union_organization = fields.Char(related='trade_union_organization.name_trade_union_organization')
    
    email = fields.Char(related='employee_id.email', string="Email công việc")

    date_of_birth = fields.Date(related="employee_id.date_of_birth",string="Ngày sinh")
    
    gender_id = fields.Many2one(related='employee_id.gender_id', string="Giới tính")
    
    place_of_birth = fields.Char(related='employee_id.place_of_birth',string="Nơi sinh")

    address = fields.Char(related='employee_id.address',string="Quê quán")

    identification = fields.Char(related='employee_id.identification',string="CMT/CCCD/Hộ chiếu")

    data_of_start_identification = fields.Date(related='employee_id.data_of_start_identification',string="Ngày cấp")

    place_of_identification = fields.Char(related='employee_id.place_of_identification',string="Nơi cấp")

    country_id = fields.Many2one(
        related='employee_id.country_id', string="Quốc tịch")

    marital_id = fields.Many2one(
        related='employee_id.marital_id', string="Tình trạng hôn nhân")

    ethnic_id = fields.Many2one(
        related='employee_id.ethnic_id', string="Dân tộc")

    religion_id = fields.Many2one(
        related='employee_id.religion_id', string="Tôn giáo")

    account_number = fields.Char(related='employee_id.account_number',string="Số tài khoản")

    account_name = fields.Char(related='employee_id.account_name',string="Tên tài khoản")

    bank = fields.Char(related='employee_id.bank',string="Ngân hàng")

    bank_branch = fields.Char(related='employee_id.bank_branch',string="Chi nhánh ngân hàng")

    personal_tax_code = fields.Char(related='employee_id.personal_tax_code',string="Mã số thuế cá nhân")

    date_of_entry_to_work = fields.Date(related='employee_id.date_of_entry_to_work',string="Ngày vào làm")

    official_start_date = fields.Date(related='employee_id.official_start_date',string="Ngày vào làm chính thức")

    high_school_level_id = fields.Many2one(
        related='employee_id.high_school_level_id',string="Trình độ phổ thông")

    highest_specialize_id = fields.Many2one(
        related='employee_id.highest_specialize_id',string="Chuyên môn cao nhất")

    study_field = fields.Char(related='employee_id.study_field',string="Lĩnh vực nghiên cứu")

    study_school = fields.Char(related='employee_id.study_school',string="Trường học")

    branch_contract = fields.Char(
        related="employee_id.branch_contract", string="Chi nhánh")
    
    department_id = fields.Many2one(related='employee_id.department_id', string="Phòng/Ban")

    job_id_contract = fields.Many2one(
        related="employee_id.job_id_contract", string="Vị trí công việc")
    
    work_location_employee = fields.Many2one(
        related="employee_id.work_location_employee", string="Địa điểm làm việc")
    
    name_contract_id = fields.Char(
        related="employee_id.name_contract_id", string="Mã hợp đồng lao động")
    
    contract_type_contract_id = fields.Many2one(
        related="employee_id.contract_type_contract_id", string="Kiểu hợp đồng lao động")
    
    wage_contract = fields.Monetary(
        related="employee_id.wage_contract", currency_field="currency_id", string="Mức lương")
    
    currency_id = fields.Many2one(
        related='employee_id.currency_id', default=lambda self: self.env.company.currency_id)
    
    phone_number = fields.Char(related='employee_id.phone_number',string="Điện thoại")

    accommodation_current = fields.Char(related='employee_id.accommodation_current',string="Chỗ ở hiện nay")

    permanent_address = fields.Char(related='employee_id.permanent_address',string="Địa chỉ thường trú")

    date_join_trade_union = fields.Date(default=fields.Date.today(), string="Ngày vào công đoàn")

    trade_union_status = fields.Selection([
            ('active', 'Hoạt động'),
            ('non', 'Không hoạt động')
        ], default='active', readonly=True, string="Tình trạng hoạt động")
    
    # START column Hr Union Terminate Operation
    active = fields.Boolean(string="Trạng thái", default=True)
    
    description_terminate_operation_union = fields.Html(string="Lý do chi tiết", copy=False, tracking=True)

    date_terminate_operation_union = fields.Date(string="Ngày ngưng hoạt động", copy=False, tracking=True)

    reason_terminate_operation_union = fields.Many2one(comodel_name="hr.trade.union.reason", string="Lý do ngừng hoạt động")

    activity_one = fields.Boolean(string="Hoạt động 1")

    activity_two = fields.Boolean(string="Hoạt động 2")

    activity_three = fields.Boolean(string="Hoạt động 3")

    activity_four = fields.Boolean(string="Hoạt động 4")
    # END column Hr Union Terminate Operation

    # END column Hr Trade Union Members
    
    
    # Active state transition function
    def toggle_active(self):
        res = super(HrTradeUnionMembers, self).toggle_active()
        unarchived_union = self.filtered(lambda employee: employee.active)
        unarchived_union.write({
            'description_terminate_operation_union': False,
            'date_terminate_operation_union': False,
            'reason_terminate_operation_union': False,
        })
        if len(self) == 1 and not self.active and not self.env.context.get('no_wizard', False):
            return {
                'type': 'ir.actions.act_window',
                'name': _('Chấm dứt hoạt động'),
                'res_model': 'hr.trade.union.terminate.operation.wizard',
                'view_mode': 'form',
                'target': 'new',
                'context': {'active_id': self.id,'default_name': self.name,},
                'views': [[False, 'form']],
            }
        return res

