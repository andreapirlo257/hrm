from odoo import _, api, fields, models


class HrTradeUnionApproval(models.Model):

    _name = "hr.trade.union.approval"
    _description = "Phê duyệt công đoàn"
    _rec_name = "name"


    # START column Trade Union Approval
    employee_id = fields.Many2one(comodel_name="hr.employee")

    name = fields.Char(related='employee_id.name', string="Hồ sơ đoàn viên", store=True)

    employee_code = fields.Char(related='employee_id.employee_code', string="Mã nhân viên")

    email = fields.Char(related='employee_id.email', string="Email công việc")

    date_of_birth = fields.Date(related="employee_id.date_of_birth",string="Ngày sinh")
    
    gender_id = fields.Many2one(related='employee_id.gender_id', string="Giới tính")
    
    place_of_birth = fields.Char(related='employee_id.place_of_birth',string="Nơi sinh")

    address = fields.Char(related='employee_id.address',string="Quê quán")

    identification = fields.Char(related='employee_id.identification',string="CMT/CCCD/Hộ chiếu")

    data_of_start_identification = fields.Date(related='employee_id.data_of_start_identification',string="Ngày cấp")

    place_of_identification = fields.Char(related='employee_id.place_of_identification',string="Nơi cấp")

    country_id = fields.Many2one(
        related='employee_id.country_id', string="Quốc tịch")

    marital_id = fields.Many2one(
        related='employee_id.marital_id', string="Tình trạng hôn nhân")

    ethnic_id = fields.Many2one(
        related='employee_id.ethnic_id', string="Dân tộc")

    religion_id = fields.Many2one(
        related='employee_id.religion_id', string="Tôn giáo")

    account_number = fields.Char(related='employee_id.account_number',string="Số tài khoản")

    account_name = fields.Char(related='employee_id.account_name',string="Tên tài khoản")

    bank = fields.Char(related='employee_id.bank',string="Ngân hàng")

    bank_branch = fields.Char(related='employee_id.bank_branch',string="Chi nhánh ngân hàng")

    personal_tax_code = fields.Char(related='employee_id.personal_tax_code',string="Mã số thuế cá nhân")

    date_of_entry_to_work = fields.Date(related='employee_id.date_of_entry_to_work',string="Ngày vào làm")

    official_start_date = fields.Date(related='employee_id.official_start_date',string="Ngày vào làm chính thức")

    high_school_level_id = fields.Many2one(
        related='employee_id.high_school_level_id',string="Trình độ phổ thông")

    highest_specialize_id = fields.Many2one(
        related='employee_id.highest_specialize_id',string="Chuyên môn cao nhất")

    study_field = fields.Char(related='employee_id.study_field',string="Lĩnh vực nghiên cứu")

    study_school = fields.Char(related='employee_id.study_school',string="Trường học")
    
    # Job Information

    branch_contract = fields.Char(
        related="employee_id.branch_contract", string="Chi nhánh")
    
    department_id = fields.Many2one(related='employee_id.department_id', string="Phòng/Ban")

    job_id_contract = fields.Many2one(
        related="employee_id.job_id_contract", string="Vị trí công việc")
    
    work_location_employee = fields.Many2one(
        related="employee_id.work_location_employee", string="Địa điểm làm việc")
    
    name_contract_id = fields.Char(
        related="employee_id.name_contract_id", string="Mã hợp đồng lao động")
    
    contract_type_contract_id = fields.Many2one(
        related="employee_id.contract_type_contract_id", string="Kiểu hợp đồng lao động")
    
    wage_contract = fields.Monetary(
        related="employee_id.wage_contract", currency_field="currency_id", string="Mức lương")
    
    currency_id = fields.Many2one(
        related='employee_id.currency_id', default=lambda self: self.env.company.currency_id)
    
    # Contact Info

    phone_number = fields.Char(related='employee_id.phone_number',string="Điện thoại")

    accommodation_current = fields.Char(related='employee_id.accommodation_current',string="Chỗ ở hiện nay")

    permanent_address = fields.Char(related='employee_id.permanent_address',string="Địa chỉ thường trú")

    image_1024 = fields.Image(related='employee_id.image_1024', compute_sudo=True)

    trade_union_state = fields.Selection([
    ('pending', 'Chờ phê duyệt'),
    ('approval', 'Đã phê duyệt')
    ], default='pending', string="Trạng thái")

    state_display = fields.Char(compute="_compute_status_display")

    trade_union_code = fields.Char(string="Mã đoàn viên")

    trade_union_position = fields.Many2one(comodel_name='hr.trade.union.department', string="Chức vụ công đoàn")

    trade_union_organization = fields.Many2one(
        comodel_name='hr.trade.union.organization', string="Tổ chức công đoàn")
    
    date_join_trade_union = fields.Date(default=fields.Date.today(), string="Ngày vào công đoàn")

    trade_union_status = fields.Selection([
            ('active', 'Hoạt động'),
            ('non', 'Không hoạt động')
        ], default='active', readonly=True, string="Tình trạng hoạt động")

    trade_union_approval_wizard_ids= fields.One2many(comodel_name="hr.trade.union.approval.wizard", inverse_name="trade_union_approval_id")

    # END column Trade Union Approval
    
    # The function to get the NAME of default_trade_union_approval_id put in hr.trade.union.approval.wizard
    def approval_hr_trade_union_member(self):
        try:
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'hr.trade.union.approval.wizard',
                'views': [(False, 'form')],
                'view_id': 'trade_union_approval_wizard_view_form',
                'target': 'new',
                'name': 'Phê duyệt đoàn viên',
                'context': {'default_name': self.name,
                            'default_trade_union_approval_id': self.id},
            }
        except Exception as e:
            raise e          
    
    @api.depends("trade_union_state")
    def _compute_status_display(self):
        for record in self:
            if record.trade_union_state and record.trade_union_state == 'approval':
                record.state_display = "Đã phê duyệt"
            else:
                record.state_display = "Chờ phê duyệt"
