from odoo import _, api, fields, models


class HrTradeUnionReason(models.Model):

    _name = "hr.trade.union.reason"
    _description = "Lý do ngừng hoạt động"
    _order = "sequence"

    # START column Hr Trade Union Reason

    sequence_number = fields.Char(readonly=True, copy=False, default=lambda self: self.env['ir.sequence'].next_by_code('my.model.sequence') or '')

    sequence = fields.Char(compute='_compute_sequence')

    name = fields.Char(string="Lý do", required=True, translate=True)

    # END column Hr Trade Union Reason

    @api.depends('sequence_number')
    def _compute_sequence(self):
        for record in self:
            record.sequence = 'MYSEQ/' + record.sequence_number
