from odoo.addons.web.models.models import Base
import odoo
import odoo.modules.registry
from odoo import http
from odoo.http import request
from odoo import _, _lt, api


class MbfAPI(odoo.http.Controller):

    @http.route('/homepage', type='http', auth="none", sitemap=False, cors='*')
    def search_mbf_app(self):
        uid = None
        model = "ir.module.module"
        fields = ["icon", "shortdesc"]
        offset = 0
        limit = 80
        domain = ["&", ["application", "=", "True"], ["state", "in", ["installed", "to upgrade", "to remove"]]]
        sort = None
        env = request.env(user=uid or request.uid or odoo.SUPERUSER_ID)
        Model = env[model]
        records = Model.web_search_read_mbf(domain, fields=fields, offset=offset, limit=limit, order=sort)
        response = http.request.render('mobifone_theme.homepage', {
            'records': records
        })

        return response

class LoginHome(Base):
    _inherit = 'base'

    @api.model
    def web_search_read_mbf(self, domain=None, fields=None, offset=0, limit=None, order=None):
        rec = self.search_read(domain, fields, offset=offset, limit=limit, order=order)
        return rec
