"""Hooks for Changing Menu Web_icon"""
import base64

from odoo import api, SUPERUSER_ID
from odoo.modules import get_module_resource


def pre_init_hook(cr):
    """pre init hook"""

    env = api.Environment(cr, SUPERUSER_ID, {})
    menu_item = env['ir.ui.menu'].search([('parent_id', '=', False)])
    # for menu in menu_item:
    #     print("menu: ", menu)


def post_init_hook(cr, registry):
    """post init hook"""

    env = api.Environment(cr, SUPERUSER_ID, {})
    menu_item = env['ir.ui.menu'].search([('parent_id', '=', False)])

    # for menu in menu_item:
    #     print("menu: ", menu)
        